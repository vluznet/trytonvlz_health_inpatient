# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelSQL, ModelView

__all__ = ['InpatientShift']


class InpatientShift(ModelSQL, ModelView):
    "Inpatient Shift"
    __name__ = "gnuhealth.inpatient.shift"
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
